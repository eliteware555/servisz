//
//  BaseTableViewController.h
//  Servisz
//
//  Created by Victory on 06/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UITableViewController {

    BOOL _isConnecting;
}

- (void) showLoadingView;
- (void) hideLoadingView;
- (void) hideLoadingView: (NSTimeInterval) delay;
- (void) showAlertDialog:(NSString *)title message:(NSString *)message positvie:(NSString *)strPositive negative:(NSString *)strNegative yesHandler: (void(^)(UIAlertAction *)) yesHandler noHandler:(void(^)(UIAlertAction *)) noHandler;
- (void) showAlertDialog:(NSString *)title message:(NSString *)message positvie:(NSString *)strPositive negative:(NSString *)strNegative;
- (CGFloat) getOffsetYWhenShowKeyboard;

@end

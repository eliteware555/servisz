//
//  BaseTableViewController.m
//  Servisz
//
//  Created by Victory on 06/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "BaseTableViewController.h"
#import "SCSkypeActivityIndicatorView.h"
@import NVActivityIndicatorView;

@interface BaseTableViewController()

@property (nonatomic, strong) SCSkypeActivityIndicatorView *activityIndicatorView;

@end

@implementation BaseTableViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    // navigation bar with custom font title
    if (self.navigationController.navigationBar != nil) {
        
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
    }
    
    // Do any additional setup after loading the view, typically from a nib.
    self.activityIndicatorView = [[SCSkypeActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/6.0, self.view.frame.size.width/6.0)];
    [self.view addSubview:self.activityIndicatorView];
    
    // activityIndicator setting
    //    [self.activityIndicatorView setNumberOfBubbles:5];
    [self.activityIndicatorView setAnimationDuration:1.8f];
    [self.activityIndicatorView setBubbleSize:CGSizeMake(8.0f, 8.0f)];
    
    self.activityIndicatorView.center = self.view.center;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) prefersStatusBarHidden {
    
    return NO;
}

// keyboard processing
- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerNotification];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self unregisterNotification];
}


- (void) registerNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void) unregisterNotification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification object:nil];
}

- (void) keyboardWillShown:(NSNotification *) notification {
    
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyBoardFrame = [keyboardFrameBegin CGRectValue];
    float offsetY = keyBoardFrame.size.height - [self getOffsetYWhenShowKeyboard];
    
    //    NSLog(@"%f", keyBoardFrame.size.height);
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -offsetY;
        self.view.frame = f;
    }];
}

- (void) keyboardWillBeHidden:(NSNotification *) notification {
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

- (void) showLoadingView {
    
    [self.activityIndicatorView startAnimating];
}

- (void) hideLoadingView {
    
    [self.activityIndicatorView stopAnimating];
}

- (void) hideLoadingView:(NSTimeInterval) delay {
    
    [self performSelector:@selector(hideLoadingView) withObject:nil afterDelay:delay];
}

- (void) showAlertDialog:(NSString *)title message:(NSString *)message positvie:(NSString *)strPositive negative:(NSString *)strNegative {
    
    NSDictionary *pinkBoldAttribtes = @{NSForegroundColorAttributeName :[UIColor colorWithRed:234/255.0 green:76/255.0 blue:85/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
    
    NSMutableAttributedString *attributedTitle;
    
    if (title != nil) {
        attributedTitle = [[NSMutableAttributedString alloc] initWithString:title];
        [attributedTitle addAttributes:pinkBoldAttribtes range:NSMakeRange(0, title.length)];
    }
    
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:message];
    [attributedMessage addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:16.0] range:NSMakeRange(0, message.length)];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if(title != nil) {
        [alert setValue:attributedTitle forKey:@"attributedTitle"];
    }
    
    [alert setValue:attributedMessage forKey:@"attributedMessage"];
    
    if(strNegative != nil) {
        
        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:strPositive
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Handel your yes please button action here
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
        
        [alert addAction:yesButton];
    }
    
    if(strNegative != nil) {
        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:strNegative
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        
        [alert addAction:noButton];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
    //    alert.view.tintColor = [UIColor darkGrayColor];
    //    alert.view.backgroundColor = [UIColor colorWithRed:71/255.0 green:74/255.0 blue:85/255.0 alpha:1.0];
}

- (void) showAlertDialog:(NSString *)title message:(NSString *)message positvie:(NSString *)strPositive negative:(NSString *)strNegative yesHandler: (void(^)(UIAlertAction *)) yesHandler noHandler:(void(^)(UIAlertAction *)) noHandler {
    
    NSDictionary *pinkBoldAttribtes = @{NSForegroundColorAttributeName :[UIColor colorWithRed:234/255.0 green:76/255.0 blue:85/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
    
    NSMutableAttributedString *attributedTitle;
    
    if (title != nil) {
        attributedTitle = [[NSMutableAttributedString alloc] initWithString:title];
        [attributedTitle addAttributes:pinkBoldAttribtes range:NSMakeRange(0, title.length)];
    }
    
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:message];
    [attributedMessage addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:16.0] range:NSMakeRange(0, message.length)];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if(title != nil) {
        [alert setValue:attributedTitle forKey:@"attributedTitle"];
    }
    
    [alert setValue:attributedMessage forKey:@"attributedMessage"];
    
    if(strNegative != nil) {
        
        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:strPositive
                                     style:UIAlertActionStyleDefault
                                     handler:yesHandler];
        [alert addAction:yesButton];
    }
    
    if(strNegative != nil) {
        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:strNegative
                                    style:UIAlertActionStyleDefault
                                    handler:noHandler];
        
        [alert addAction:noButton];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (CGFloat) getOffsetYWhenShowKeyboard {
    
    return 0.0;
}

@end

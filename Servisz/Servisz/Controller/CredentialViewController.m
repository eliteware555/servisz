//
//  CredentialViewController.m
//  Servisz
//
//  Created by Victory on 05/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "CredentialViewController.h"
#import "CommonUtils.h"
#import "CRGradientNavigationBar.h"
//#import "HomeViewController.h"
#import "RootViewController.h"


@interface CredentialViewController() <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIButton *btnAction; // Singup, Login, Forgot Password

@property (nonatomic, weak) IBOutlet UIButton *btnAlreadySignup;
@property (nonatomic, weak) IBOutlet UIButton *btnNewSignup;
@property (nonatomic, weak) IBOutlet UIButton *btnForgotPassword;

// Signup View
@property (nonatomic, weak) IBOutlet UIView *viewSignup;

@property (nonatomic, weak) IBOutlet UITextField *txtSignupEmailField;
@property (nonatomic, weak) IBOutlet UITextField *txtSignupNameField;
@property (nonatomic, weak) IBOutlet UITextField *txtSignupPasswordField;

@property (nonatomic, weak) IBOutlet UIButton *btnFreelancer;
@property (nonatomic, weak) IBOutlet UIButton *btnEmployeer;


// Login View
@property (nonatomic, weak) IBOutlet UIView *viewLogin;

@property (nonatomic, weak) IBOutlet UITextField *txtLoginEmailField;
@property (nonatomic, weak) IBOutlet UITextField *txtLoginPasswordField;

// ForgotPassword View
@property (nonatomic, weak) IBOutlet UIView *viewForgotPassword;

@property (nonatomic, weak) IBOutlet UITextField *txtForgotEmailField;

@end

@implementation CredentialViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    // action button border color
    _btnAction.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self showCurrentView:_kindOfView];
    
    // hire, work button border color
    _btnEmployeer.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnFreelancer.layer.borderColor = [UIColor whiteColor].CGColor;

    [self updateLooking:NO];
    
    // customize TextField
    
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) showCurrentView:(int) nIndex {
    
    if (nIndex == VIEW_SIGNUP) {
        
        // hide LoginView and Forgot Password View
        [UIView animateWithDuration:0.5f animations:^{
            
            // hide view with alpha control
            self.viewForgotPassword.alpha = 0.0f;
            self.viewLogin.alpha = 0.0f;
            
            // hide button with alpha control
            self.btnForgotPassword.alpha = 0.0f;
            self.btnNewSignup.alpha = 0.0f;
            
            self.btnAction.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
            
            self.viewForgotPassword.hidden = YES;
            self.viewLogin.hidden = YES;
            
            // hide button
            self.btnForgotPassword.hidden = YES;
            self.btnNewSignup.hidden = YES;
            
            self.viewSignup.alpha = 0.0f;
            self.viewSignup.hidden = NO;
            
            self.btnAlreadySignup.alpha = 0.0f;
            self.btnAlreadySignup.hidden = NO;
            
            [_btnAction setTitle:@"SIGN UP" forState:UIControlStateNormal];
            
            [UIView animateWithDuration:0.5f animations:^{
                
                // show view
                self.viewSignup.alpha = 1.0f;
                
                // show button
                self.btnAlreadySignup.alpha = 1.0f;
                
                self.btnAction.alpha = 1.0f;
                
            } completion:nil];
        }];
        
        _kindOfView = VIEW_SIGNUP;
        
    } else if (nIndex == VIEW_LOGIN) {
        
        // hide LoginView and Forgot Password View
        [UIView animateWithDuration:0.5f animations:^{
            
            // hide view with alpha control
            self.viewForgotPassword.alpha = 0.0f;
            self.viewSignup.alpha = 0.0f;
            
            // hide button with alpha control
            self.btnAlreadySignup.alpha = 0.0f;
            
            self.btnAction.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
            
            self.viewForgotPassword.hidden = YES;
            self.viewSignup.hidden = YES;
            
            // hide already sign up button
            self.btnAlreadySignup.hidden = YES;
            
            // show login view
            self.viewLogin.alpha = 0.0f;
            self.viewLogin.hidden = NO;
            
            // show new user sign up & forgot password button
            self.btnNewSignup.alpha = 0.0f;
            self.btnNewSignup.hidden = NO;
            
            NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"New user? Singup"];
            // making text property to underline text-
//             [titleString setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(10, 6)];
            [titleString setAttributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(10, 6)];
            [titleString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, titleString.length)];
            // using text on button
            [self.btnNewSignup setAttributedTitle: titleString forState:UIControlStateNormal];
            
            self.btnForgotPassword.alpha = 0.0f;
            self.btnForgotPassword.hidden = NO;
            
            [self.btnForgotPassword setAttributedTitle:[[NSMutableAttributedString alloc] initWithString:@""] forState:UIControlStateNormal];
            
            [self.btnForgotPassword setTitle:@"Forgot Password?" forState:UIControlStateNormal];
            
            [_btnAction setTitle:@"LOG IN" forState:UIControlStateNormal];
            
            [UIView animateWithDuration:0.5f animations:^{
                
                self.viewLogin.alpha = 1.0f;
                
                self.btnForgotPassword.alpha = 1.0f;
                self.btnNewSignup.alpha = 1.0f;
                
                self.btnAction.alpha = 1.0f;
        
                
            } completion:nil];
        }];
        
        _kindOfView = VIEW_LOGIN;
        
    } else {
        
        // hide LoginView and Forgot Password View
        [UIView animateWithDuration:0.5f animations:^{
            
            // hide view with alpha control
            self.viewLogin.alpha = 0.0f;
            self.viewSignup.alpha = 0.0f;
            
            // hide button with alpha control
            self.btnAlreadySignup.alpha = 0.0f;
            
            self.btnAction.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
            
            // hide view
            self.viewLogin.hidden = YES;
            self.viewSignup.hidden = YES;
            
            // hide button
            self.btnAlreadySignup.hidden = YES;
            
            self.viewForgotPassword.alpha = 0;
            self.viewForgotPassword.hidden = NO;
            
            // show new user sign up & forgot password button
            self.btnNewSignup.alpha = 0.0f;
            self.btnNewSignup.hidden = NO;
            
            [self.btnNewSignup setAttributedTitle:[[NSMutableAttributedString alloc] initWithString:@""] forState:UIControlStateNormal];
            [self.btnNewSignup setTitle:@"Back to login" forState:UIControlStateNormal];
            
            NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"New user? Singup"];
            [titleString setAttributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(10, 6)];
            [titleString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, titleString.length)];
            // using text on button
            [self.btnForgotPassword setAttributedTitle: titleString forState:UIControlStateNormal];
            
            self.btnForgotPassword.alpha = 0.0f;
            self.btnForgotPassword.hidden = NO;
            
            [_btnAction setTitle:@"RESET PASSWORD" forState:UIControlStateNormal];
            
            [UIView animateWithDuration:0.5f animations:^{
                
                self.viewForgotPassword.alpha = 1.0f;
                
                self.btnNewSignup.alpha = 1.0f;
                self.btnForgotPassword.alpha = 1.0f;
                
                self.btnAction.alpha = 1.0f;
                
            } completion:nil];
        }];
        
        
        
        _kindOfView = VIEW_FORGOT_PASSWORD;
    }
}

- (IBAction) selectRole:(UIButton *)sender {
    
    if (sender.tag == 200) {
        
        [self updateLooking:NO];
        
    } else {
        
        [self updateLooking:YES];
    }
}

- (void) updateLooking: (BOOL) isWork {
    
    if (isWork) {
        
        // freelancer
        _btnFreelancer.layer.borderWidth = 0;
        _btnFreelancer.backgroundColor =[UIColor colorWithRed:129/255.0 green:199/255.0 blue:132/255.0 alpha:1.0];
        
        _btnEmployeer.layer.borderWidth = 1;
        _btnEmployeer.backgroundColor = [UIColor clearColor];
        
    } else {
        
        // employeer
        _btnEmployeer.layer.borderWidth = 0;
        _btnEmployeer.backgroundColor =[UIColor colorWithRed:102/255.0 green:187/255.0 blue:106/255.0 alpha:1.0];
        
        _btnFreelancer.layer.borderWidth = 1;
        _btnFreelancer.backgroundColor = [UIColor clearColor];
    }
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (CGFloat) getOffsetYWhenShowKeyboard {
    
    if (_kindOfView == VIEW_SIGNUP) {
        
        return 216.0;
        
    } else if (_kindOfView == VIEW_LOGIN) {
        
        return 140.0;
        
    } else {
        
        return 140.0;
    }
}

#pragma mark - 
#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    switch (_kindOfView) {
            
        case VIEW_SIGNUP:
            
            if (textField == _txtSignupEmailField) {
                
                [_txtSignupNameField becomeFirstResponder];
                
            } else if (textField == _txtSignupNameField) {
                
                [_txtSignupPasswordField becomeFirstResponder];
            }
            
            break;
            
        case VIEW_LOGIN:
            
            if (textField == _txtLoginEmailField) {
                
                [_txtLoginPasswordField becomeFirstResponder];
            }
            
            break;
            
        default:
            
            break;
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

// Touch Up Inside
- (IBAction) newSignupTapped:(id)sender {
    
    if (_kindOfView == VIEW_LOGIN) {
    
        [self showCurrentView:VIEW_SIGNUP];
        
    } else {
        
        [self showCurrentView:VIEW_LOGIN];
    }
}

- (IBAction) ForgotPasswordTapped:(id)sender {
    
    if (_kindOfView == VIEW_LOGIN) {
    
        [self showCurrentView:VIEW_FORGOT_PASSWORD];
        
    } else {
        
        [self showCurrentView:VIEW_SIGNUP];
    }
}

- (IBAction) alreadyAccountTapped:(id)sender {
    
    [self showCurrentView:VIEW_LOGIN];
}

- (IBAction) actionTapped:(UIButton *)sender {
    
    
    switch (_kindOfView) {
            
        case VIEW_SIGNUP:
            
            [self showLoadingView];
            
//            [self gotoMainView];
            
            [self performSelector:@selector(gotoMainView) withObject:[NSNumber numberWithBool:YES] afterDelay:1.0f];
        
            break;
            
        case VIEW_LOGIN:
            
            [self showLoadingView];
            
            [self performSelector:@selector(gotoMainView) withObject:[NSNumber numberWithBool:YES] afterDelay:1.0f];
            
//             [self gotoMainView];
            
            break;
            
        default:
            break;
    }
}

- (void) gotoMainView {
    
//    UINavigationController *mainNav = [[UINavigationController alloc] initWithNavigationBarClass:[CRGradientNavigationBar class] toolbarClass:nil];
//    
//    UIColor *firstColor = [UIColor colorWithRed:243/255.0f green:178/255.0f blue:28/255.0f alpha:1.0f];
//    UIColor *secondColor = [UIColor colorWithRed:247/255.0f green:200/255.0f blue:110/255.0f alpha:1.0f];
//    
//    NSArray *colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];
//    
//    [[CRGradientNavigationBar appearance] setBarTintGradientColors:colors];
//    [[mainNav navigationBar] setTranslucent:NO]; // Remember, the default value is YES.
//    
//    // remove navigation bar 1px bottom line
//    UIImage *image = [[UIImage alloc] init];
//    
//    [mainNav.navigationBar setShadowImage:image];
//    [mainNav.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
//    
//    HomeViewController *viewController = (HomeViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    [mainNav setViewControllers:@[viewController]];
//    
//    [[UIApplication sharedApplication].keyWindow setRootViewController:mainNav];
    
    [self hideLoadingView];
    
    RootViewController *rootVC = (RootViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"RootViewController"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootVC];
}

@end





//
//  BrowseJobViewController.m
//  Servisz
//
//  Created by Victory on 07/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "BrowseJobViewController.h"
#import "BrowseProjectCell.h"
#import "CommonUtils.h"

@interface BrowseJobViewController() <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate> {
    
    BOOL searchActive;

    // activityIndicator on UIBarButtonItem
    UIActivityIndicatorView *activityIndicator;
    
}

@property (nonatomic, weak) IBOutlet UISegmentedControl *filterSegment;

@property (nonatomic, strong) IBOutlet UIBarButtonItem *refreshButtonItem;

@property (nonatomic, weak) IBOutlet UITableView *projectsList;

@property (nonatomic, weak) IBOutlet UISearchBar *searchField;

@end

@implementation BrowseJobViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    // navigation bar with custom font title
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
 
    
    // navigation item title
    self.navigationItem.title = @"Browse Job";
    
    // gradient background
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = CGRectMake(0, 0, self.view.bounds.size.width, 88);
    
    // horizontal gradient
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    UIColor *firstColor = [UIColor colorWithRed:243/255.0f green:178/255.0f blue:28/255.0f alpha:1.0f];
    UIColor *secondColor = [UIColor colorWithRed:247/255.0f green:200/255.0f blue:110/255.0f alpha:1.0f];
    
    gradient.colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    // customize segment control
    _filterSegment.layer.borderColor = [UIColor whiteColor].CGColor;
    [_filterSegment setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [_filterSegment setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]} forState:UIControlStateNormal];
    
    // remove empty cell
    self.projectsList.tableFooterView = [[UIView alloc] init];
    
    // auto resize
    self.projectsList.estimatedRowHeight = 168.0f;
    self.projectsList.rowHeight = UITableViewAutomaticDimension;
    
    // customize searchbar
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setBackgroundColor:[UIColor colorWithRed:243/255.0f green:178/255.0f blue:28/255.0f alpha:1.0f]];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTextColor:[UIColor whiteColor]];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) stopAnimating {
    
    [activityIndicator stopAnimating];
    
    self.navigationItem.rightBarButtonItem = self.refreshButtonItem;
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

// ------------------------------------------------------------------
#pragma mark - UISearchBarDelegate
// ------------------------------------------------------------------
- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = YES;
}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = NO;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
    _searchField.showsCancelButton = NO;
    
    searchBar.text = @"";
    
//    if(searchActive) {
//        searchActive = NO;
//        [tblUserList reloadData];
//    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
    
    _searchField.showsCancelButton = NO;
    
//    if(searchBar.text.length > 0) {
//        
//        searchActive
//        
//        [self refreshAction:_refreshButtonItem];
//    }
}

#pragma mark - UITableViewDataSource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"BrowseProjectCell";
    
    BrowseProjectCell *cell = (BrowseProjectCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    return cell;
}



#pragma mark - IBAction
- (IBAction) refreshAction:(id)sender {
    
    if (activityIndicator == nil) {
        
        activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    }
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    
    // set to Right Bar button item
    [[self navigationItem] setRightBarButtonItem:barButton];
    [activityIndicator startAnimating];
    
    // for testing
    [self performSelector:@selector(stopAnimating) withObject:nil afterDelay:3.0];
}

@end

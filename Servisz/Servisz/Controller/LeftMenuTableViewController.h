//
//  LeftMenuTableViewController.h
//  Servisz
//
//  Created by Victory on 07/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface LeftMenuTableViewController : UITableViewController <RESideMenuDelegate>

- (void) updateSelectionState: (NSIndexPath *) newSelectedIndexPath;

@end

//
//  IntroViewController.m
//  Servisz
//
//  Created by Victory on 03/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "IntroViewController.h"
#import "CommonUtils.h"
#import "CredentialViewController.h"

@interface IntroViewController()

@property (nonatomic, weak) IBOutlet UIButton *btnFacebook;
@property (nonatomic, weak) IBOutlet UIButton *btnLogin;
@property (nonatomic, weak) IBOutlet UIButton *btnSignup;

@end

@implementation IntroViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {

    _btnSignup.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _btnLogin.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"SegueIntro2Signup"]) {
        
        CredentialViewController *destVC = (CredentialViewController *) [segue destinationViewController];
        
        destVC.kindOfView = VIEW_SIGNUP;
        
    } else if ([segue.identifier isEqualToString:@"SegueIntro2Login"]) {
        
        CredentialViewController *destVC = (CredentialViewController *) [segue destinationViewController];
        
        destVC.kindOfView = VIEW_LOGIN;
    }
    
}

@end

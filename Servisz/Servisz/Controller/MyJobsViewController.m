//
//  MyJobViewController.m
//  Servisz
//
//  Created by Victory on 07/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "MyJobsViewController.h"
#import "ProjectCell.h"
@import SWSegmentedControl;

@interface MyJobsViewController() <UITableViewDataSource, UITableViewDelegate> {
    
    // activityIndicator on UIBarButtonItem
    UIActivityIndicatorView *activityIndicator;
    
    // refresh controller
    UIRefreshControl *refreshControl;
    
    int segmentIndex;
}

@property (nonatomic, strong) IBOutlet UIBarButtonItem *refreshButtonItem;

@property (nonatomic, weak) IBOutlet UIView *viewSegment;

@property (nonatomic, weak) IBOutlet UITableView *jobsList;

@end

@implementation MyJobsViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    // navigation item title
    self.navigationItem.title = @"My Jobs";
    
    // gradient background
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = CGRectMake(0, 0, self.view.bounds.size.width, 64);
    
    // horizontal gradient
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    UIColor *firstColor = [UIColor colorWithRed:243/255.0f green:178/255.0f blue:28/255.0f alpha:1.0f];
    UIColor *secondColor = [UIColor colorWithRed:247/255.0f green:200/255.0f blue:110/255.0f alpha:1.0f];
    
    gradient.colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    // customize segmentControl and add to view
    SWSegmentedControl *segmentedControl = [[SWSegmentedControl alloc] initWithItems:@[@"In Progress", @"Past Projects"]];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
//    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    segmentedControl.selectedSegmentIndex = 0;
    segmentedControl.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    
    segmentedControl.titleColor = [UIColor whiteColor];
    segmentedControl.unselectedTitleColor = [UIColor colorWithRed:217/255.0 green:217/255.0 blue:217/255.0 alpha:1.0];
    segmentedControl.indicatorColor = [UIColor colorWithRed:255/255.0 green:152/255.0 blue:0/255.0 alpha:1.0];
    
    [segmentedControl addTarget:self action:@selector(segmentedChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.viewSegment addSubview:segmentedControl];
    
    segmentIndex = 0;
    
    // remove empty cell
    self.jobsList.tableFooterView = [[UIView alloc] init];
    
    // add refresh controller in tableview
    refreshControl = [[UIRefreshControl alloc] init];
    [self.jobsList addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - 
#pragma mark - Refresh Control Action
- (void) refreshTable {
    
    //TODO: refresh your data
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        //        [self loadRecentMessage];
        
        [refreshControl endRefreshing];
    });
}

- (void) stopAnimating {
    
    [activityIndicator stopAnimating];
    
    self.navigationItem.rightBarButtonItem = self.refreshButtonItem;    
}

#pragma mark - UITableViewDataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"MyJobsCell";
    
    ProjectCell *cell = (ProjectCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (indexPath.row == 0) {
        
        cell.lblPostedTime.text = @" 2 days ago";
        cell.lblResult.text = @"completed";
        
    } else {
        
        cell.lblPostedTime.text = @" 20 days ago";
        cell.lblResult.text = @"closed";
    }
    
    if (segmentIndex == 0) {
        
        cell.lblResult.hidden = YES;
    } else {
        
        cell.lblResult.hidden = NO;
    }
    
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 170.f;
}

#pragma mark - IBAction
- (IBAction) refreshAction:(id)sender {

    if (activityIndicator == nil) {
    
        activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    }
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    
    // set to Right Bar button item
    [[self navigationItem] setRightBarButtonItem:barButton];
    [activityIndicator startAnimating];
    
    // for testing
    [self performSelector:@selector(stopAnimating) withObject:nil afterDelay:3.0];
}

- (void) segmentedChanged:(SWSegmentedControl *)sender {
    
//    NSLog(@"selected Segmented IndexPath: %d", sender.selectedSegmentIndex);
    
    segmentIndex = sender.selectedSegmentIndex;
    
    [self.jobsList reloadData];
}

@end






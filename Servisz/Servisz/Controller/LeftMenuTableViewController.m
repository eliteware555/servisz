//
//  LeftMenuTableViewController.m
//  Servisz
//
//  Created by Victory on 07/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "LeftMenuTableViewController.h"
#import "NZCircularImageView.h"
#import "SideMenuCell.h"

@interface LeftMenuTableViewController() {

    NSIndexPath *selectedIndexPath;
}

// user information
@property (nonatomic, weak) IBOutlet NZCircularImageView *imvUserImage;
@property (nonatomic, weak) IBOutlet NZCircularImageView *imvRoundRect;

@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblCountry;

@end

@implementation LeftMenuTableViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    selectedIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    
    self.tableView.tableFooterView = [UIView new];
    
    // fill out user information
    _imvUserImage.borderColor = [UIColor whiteColor];
    _imvUserImage.borderWidth = [NSNumber numberWithInt:3];
    
    _imvRoundRect.borderColor = [UIColor whiteColor];
    _imvRoundRect.borderWidth = [NSNumber numberWithInt:1];
    
    _lblName.text = @"John Doe";
    _lblCountry.text = @"Austalia";
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0 || indexPath.row == 3 || indexPath.row == 8 || indexPath.row == 10 || indexPath.row == 14) {
        
        return;
    }
    
    if (selectedIndexPath.row == indexPath.row) {
        
        // toggle side menu
        
        [self.sideMenuViewController hideMenuViewController];
        
        return;
        
    } else {
        
        // update selection mark        
        [self updateSelectionState:indexPath];
        
        NSString *storyboardId = @"";
        
        switch (indexPath.row) {
            case 1:
                
                // HomeViewController - Home
                storyboardId = @"HomeNav";
                
                break;
                
            case 2:
                
                // FundsViewController
                storyboardId = @"FundsNav";
                
                break;
                
            case 4:
                
                // MyJobsViewController - My Jobs
                storyboardId = @"MyJobsNav";
                
                break;
                
            case 5:
                
                // MyBidsViewController - My Bids
                storyboardId = @"MyBidsNav";
                
                break;
                
            case 6:
                
                // BrowseJobViewController - Browse Jobs
                storyboardId = @"BrowseJobNav";
                
                break;
                
            case 7:
                
                // PostJobViewController - Post a Job
                storyboardId = @"PostJobNav";
                
                break;
                
            case 9:
                
                // MessageListViewController - Message
                storyboardId = @"MessageListNav";
                
                break;
                
            case 11:
                
                //AccountViewController - Account Setting
                storyboardId = @"AccountNav";
                
                break;
                
            case 12:
                
                // AboutViewController - About
                storyboardId = @"AboutNav";
                
                break;
                
            case 13:
                
                // Log out
                
                // clear user credentials save in UserDefault
                
                // go to IntroViewController
                [[UIApplication sharedApplication].keyWindow setRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"IntroViewController"]];
                
                return;
                
            default:
                return;
        }
        
        [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:storyboardId] animated:YES];
        
        selectedIndexPath = indexPath;
        
        [self.sideMenuViewController hideMenuViewController];
    }
}

- (void) updateSelectionState: (NSIndexPath *) newSelectedIndexPath {
    
    // clear selection
    SideMenuCell *cell = (SideMenuCell *)[self.tableView cellForRowAtIndexPath:selectedIndexPath];
    cell.lblSelectMark.backgroundColor = [UIColor clearColor];
    
    // mark new selection
    SideMenuCell *newCell = (SideMenuCell *) [self.tableView cellForRowAtIndexPath:newSelectedIndexPath];
    newCell.lblSelectMark.backgroundColor = [UIColor colorWithRed:225/255.0 green:166/255.0 blue:38/255.0 alpha:1.0];
    
    selectedIndexPath = newSelectedIndexPath;
}

@end

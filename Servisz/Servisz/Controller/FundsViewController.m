//
//  FundsTableViewController.m
//  Servisz
//
//  Created by Victory on 07/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "FundsViewController.h"

@interface FundsViewController() <UITextFieldDelegate> {

    UIRefreshControl *refreshControl;
}

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UIView *fundsView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentSizeH;

@property (nonatomic, weak) IBOutlet UILabel *lblMyFunds;

@end


@implementation FundsViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    // navigation item title
    self.navigationItem.title = @"My Funds";
    
    // gradient background
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = CGRectMake(0, 0, self.view.bounds.size.width, 200);
    
    // horizontal gradient
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    UIColor *firstColor = [UIColor colorWithRed:243/255.0f green:178/255.0f blue:28/255.0f alpha:1.0f];
    UIColor *secondColor = [UIColor colorWithRed:247/255.0f green:200/255.0f blue:110/255.0f alpha:1.0f];
    
    gradient.colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    CAGradientLayer *gradientfunds = [CAGradientLayer layer];
    
    gradientfunds.frame = CGRectMake(0, 0, self.view.bounds.size.width, 200);
    
    // horizontal gradient
    gradientfunds.startPoint = CGPointMake(0.0, 0.5);
    gradientfunds.endPoint = CGPointMake(1.0, 0.5);
    
    gradientfunds.colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];
    [self.fundsView.layer insertSublayer:gradientfunds atIndex:0];
    
    // refresh control
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [self.scrollView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];

    // change scrollView height for scrolling on every device
    _contentSizeH.constant = self.view.bounds.size.height;
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (CGFloat) getOffsetYWhenShowKeyboard {
    
    return 100;
}

#pragma mark - Refresh Control Action

- (void) refreshTable {
    
    //TODO: refresh your data
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
//        [self loadRecentMessage];
        
        [refreshControl endRefreshing];
    });
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - IBAction

// fundsViewTapped
- (IBAction) fundsViewTapped:(id)sender {
    
    
    [self.view endEditing:YES];
}

// currencyTapped
- (IBAction) currencyTapped:(id)sender {
    
    [self.view endEditing:YES];
}

- (IBAction) depositTapped:(id)sender {
    
    [self.view endEditing:YES];
}

- (IBAction) paymentMothodTapped:(id)sender {
    
    [self.view endEditing:YES];
}

@end




//
//  MessageListViewController.m
//  Servisz
//
//  Created by Victory on 07/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "MessageListViewController.h"

@implementation MessageListViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    // navigation item title
    self.navigationItem.title = @"Messages";
    
    // gradient background
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    
    // horizontal gradient
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    UIColor *firstColor = [UIColor colorWithRed:243/255.0f green:178/255.0f blue:28/255.0f alpha:1.0f];
    UIColor *secondColor = [UIColor colorWithRed:247/255.0f green:200/255.0f blue:110/255.0f alpha:1.0f];
    
    gradient.colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

@end

//
//  HomeViewController.m
//  Servisz
//
//  Created by Victory on 05/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "HomeViewController.h"
#import "NZCircularImageView.h"
#import "M13BadgeView.h"
#import "NSString+FontAwesome.h"
#import "TKRoundedView.h"
#import "RESideMenu.h"
#import "LeftMenuTableViewController.h"

@interface HomeViewController()

// user profile image
@property (nonatomic, weak) IBOutlet UIView *viewProfile;
@property (nonatomic, weak) IBOutlet NZCircularImageView *imvUserImage;
@property (nonatomic, weak) IBOutlet NZCircularImageView *imvRoundRect;

// user name and country label
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblCountry;

// message and notification icon and badge view
@property (nonatomic, weak) IBOutlet UIView *messageSuperView;
@property (nonatomic, retain) M13BadgeView *messageView;

@property (nonatomic, weak) IBOutlet UIView *notiSuperView;
@property (nonatomic, retain) M13BadgeView *notiView;

// my jobs and bids
@property (nonatomic, weak) IBOutlet UIButton *btnMyJobs;
@property (nonatomic, weak) IBOutlet UIButton *btnMyBids;

// search job
@property (nonatomic, weak) IBOutlet UILabel *lblSearchIcon;

// post job button
@property (nonatomic, weak) IBOutlet UIButton *btnPostJob;
@property (nonatomic, weak) IBOutlet UIButton *btnAdd;
@property (nonatomic, weak) IBOutlet UIButton *btnManage;

@property (nonatomic, weak) IBOutlet UILabel *lblBalance;
@property (nonatomic, weak) IBOutlet UILabel *lblUnit;


@end

@implementation HomeViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];

    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    [self initView];
}

- (void) initView {
    
    // navitaion item title
    self.navigationItem.title = @"Home";
    
    // gradient background
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height * 240/600.0f);
    
    // horizontal gradient
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    UIColor *firstColor = [UIColor colorWithRed:243/255.0f green:178/255.0f blue:28/255.0f alpha:1.0f];
    UIColor *secondColor = [UIColor colorWithRed:247/255.0f green:200/255.0f blue:110/255.0f alpha:1.0f];
    
    gradient.colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    // badge view
    _messageView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 20.0, 20.0)];
    _messageView.text = @"23";
    [_messageSuperView addSubview:_messageView];
    
    _messageView.hidesWhenZero = YES;
    _messageView.maximumWidth = 20.0;
    _messageView.font = [UIFont systemFontOfSize:13.0];
    _messageView.horizontalAlignment = M13BadgeViewHorizontalAlignmentRight;
    _messageView.verticalAlignment = M13BadgeViewVerticalAlignmentBottom;

    
    _notiView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 20.0, 20.0)];
    _notiView.text = @"1";
    [_notiSuperView addSubview:_notiView];
    
    _notiView.hidesWhenZero = YES;
    _notiView.maximumWidth = 20.0;
    _notiView.font = [UIFont systemFontOfSize:13.0];
    _notiView.verticalAlignment = M13BadgeViewVerticalAlignmentBottom;
    _notiView.horizontalAlignment = M13BadgeViewHorizontalAlignmentLeft;
    
    // my jobs and my bids
    _btnMyJobs.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:20];
    
    NSDictionary *whiteBoldAttributes = @{NSForegroundColorAttributeName :[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:20.0]};
    
    NSMutableAttributedString *strMyJobs = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"My Jobs    %@", [NSString awesomeIcon:FaChevronRight]]];
    
    [strMyJobs addAttributes:whiteBoldAttributes range:NSMakeRange(0, 7)];
    [self.btnMyJobs setAttributedTitle:strMyJobs forState:UIControlStateNormal];
    
    _btnMyBids.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:20];
    
    NSMutableAttributedString *strMyBids = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"My Bids    %@", [NSString awesomeIcon:FaChevronRight]]];
    
    [strMyBids addAttributes:whiteBoldAttributes range:NSMakeRange(0, 7)];
    [self.btnMyBids setAttributedTitle:strMyBids forState:UIControlStateNormal];
    
    // add & manage button
    _btnAdd.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:20];
    
    NSDictionary *whiteBoldFont16Attributes = @{NSForegroundColorAttributeName :[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
    
    NSMutableAttributedString *strAdd = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@    Add", [NSString awesomeIcon:FaPlus]]];
    
    [strAdd addAttributes:whiteBoldFont16Attributes range:NSMakeRange(strAdd.length - 3, 3)];
    [self.btnAdd setAttributedTitle:strAdd forState:UIControlStateNormal];
    
    _btnManage.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:20];
    
    NSMutableAttributedString *strManage = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@    Manage", [NSString awesomeIcon:FaListUl]]];
    
    [strManage addAttributes:whiteBoldFont16Attributes range:NSMakeRange(strAdd.length - 6, 6)];
    [self.btnManage setAttributedTitle:strManage forState:UIControlStateNormal];
    
    self.lblSearchIcon.font = [UIFont fontWithName:@"FontAwesome" size:25.0];
    self.lblSearchIcon.text = [NSString awesomeIcon:FaSearch];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (IBAction) myJobsTapped:(id)sender {
    
    // update left menu selection state
    LeftMenuTableViewController *leftMenuVC = (LeftMenuTableViewController *) self.sideMenuViewController.leftMenuViewController;
    [leftMenuVC updateSelectionState:[NSIndexPath indexPathForRow:4 inSection:0]];
    
    // go to BrowseJobViewController
    [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MyJobsNav"] animated:YES];
}

- (IBAction) myBidsTapped:(id)sender {
    
    // update left menu selection state
    LeftMenuTableViewController *leftMenuVC = (LeftMenuTableViewController *) self.sideMenuViewController.leftMenuViewController;
    [leftMenuVC updateSelectionState:[NSIndexPath indexPathForRow:5 inSection:0]];
    
    // go to BrowseJobViewController
    [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MyBidsNav"] animated:YES];
}

- (IBAction) myFundsTapped:(id)sender {
    
    // update left menu selection state
    LeftMenuTableViewController *leftMenuVC = (LeftMenuTableViewController *) self.sideMenuViewController.leftMenuViewController;
    [leftMenuVC updateSelectionState:[NSIndexPath indexPathForRow:2 inSection:0]];
    
    // go to BrowseJobViewController
    [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"FundsNav"] animated:YES];
}

- (IBAction) addTapped:(id)sender {
    
    
}

- (IBAction) manageTapped:(id)sender {
    
}

- (IBAction) postJobTapped:(id)sender {
    
    // update left menu selection state
    LeftMenuTableViewController *leftMenuVC = (LeftMenuTableViewController *) self.sideMenuViewController.leftMenuViewController;
    [leftMenuVC updateSelectionState:[NSIndexPath indexPathForRow:7 inSection:0]];
    
    // go to BrowseJobViewController
    [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"PostJobNav"] animated:YES];
}

- (IBAction) browseJobTapped:(id)sender {
    
    // update left menu selection state
    LeftMenuTableViewController *leftMenuVC = (LeftMenuTableViewController *) self.sideMenuViewController.leftMenuViewController;
    [leftMenuVC updateSelectionState:[NSIndexPath indexPathForRow:6 inSection:0]];
    
    // go to BrowseJobViewController
    [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"BrowseJobNav"] animated:YES];
}

@end




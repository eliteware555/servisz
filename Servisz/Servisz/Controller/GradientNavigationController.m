//
//  GradientNavigationController.m
//  Servisz
//
//  Created by Victory on 07/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "GradientNavigationController.h"
#import "CRGradientNavigationBar.h"

@implementation GradientNavigationController

- (void) awakeFromNib {
    
    // Gradient Navigation Bar
    UIColor *firstColor = [UIColor colorWithRed:243/255.0f green:178/255.0f blue:28/255.0f alpha:1.0f];
    UIColor *secondColor = [UIColor colorWithRed:247/255.0f green:200/255.0f blue:110/255.0f alpha:1.0f];

    NSArray *colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];

    [[CRGradientNavigationBar appearance] setBarTintGradientColors:colors];
    
    // Remember, the default value is YES.
    [self.navigationBar setTranslucent:NO];

    // remove navigation bar 1px bottom line
    UIImage *image = [[UIImage alloc] init];

    [self.navigationBar setShadowImage:image];
    [self.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
}

@end

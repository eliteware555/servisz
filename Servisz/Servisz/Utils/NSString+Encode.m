//
//  NSString+Encode.m
//  CampusGlue
//
//  Created by victory on 3/23/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "NSString+Encode.h"

@implementation NSString (encode)

- (NSString *)encodeString:(NSStringEncoding)encoding
{
    return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self,
                                                                NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                CFStringConvertNSStringEncodingToEncoding(encoding)));
}

@end

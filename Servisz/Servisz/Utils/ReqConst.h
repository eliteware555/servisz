//
//  ReqConst.h
//  DChat
//
//  Created by ChenJunLi on 12/16/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#ifndef ReqConst_h
#define ReqConst_h

/**
 **     Server URL Macro
 **/
#pragma mark -
#pragma mark - Server URL

#define XMPP_RESOURCE                   @"campus"

#define XMPP_SERVER_URL                 @"52.34.222.14"

#define SERVER_BASE_URL                 @"http://52.34.222.14"


#define SERVER_URL [NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, @"/index.php/api/"]

#define REQ_SINGUP                      @"signup"
#define REQ_UPLOADIMAGE                 @"uploadImage"
#define REQ_LOGIN                       @"login"
#define REQ_UPDATEPROFILE               @"updateProfile"
#define REQ_GETALLUSERS                 @"getAllUsers"
#define REQ_ADDFRIEND                   @"addFriend"
#define REQ_GETFRIENDS                  @"getFriends"
#define REQ_GETCLASS                    @"getClass"
#define REQ_GETCLASSMATE                @"getClassmate"
#define REQ_ADDCLASS                    @"addClass"
#define REQ_SEARCHUSERS                 @"searchUsers"
#define REQ_GETAUTHCODE                 @"getAuthCode"
#define REQ_DELETECLASS                 @"deleteClass"
#define REQ_DELETEFRIEND                @"deleteFriend"
#define REQ_RESETPWD                    @"resetPassword"
#define REQ_GETUSERINFO                 @"getUserInfo"

#define REQ_GETVERIFYCODE               @"getVerifyCode"
#define REQ_EMAILCONFIRM                @"emailConfirm"

#define REQ_GETBLOCKLIST                @"getBlockList"

#define REQ_SETBLOCKSTATUS              @"setBlockStatus"

#define REQ_REPORTUSER                  @"reportUser"

#define REQ_BLOCKUSER                   1   // blocked user
#define REQ_UNBLOCKUSER                 0   // unblocked user



/**
 **     Response Params
 **/
#pragma mark -
#pragma mark - Response Params

#define RES_CODE                        @"result_code"
#define RES_USERINFO                    @"user_info"
#define RES_USERINFOS                   @"user_infos"
#define RES_ID                          @"id"
#define RES_FIRSTNAME                   @"first_name"
#define RES_LASTNAME                    @"last_name"
#define RES_MAJOR                       @"major"
#define RES_YEAR                        @"year"
#define RES_SEX                         @"sex"
#define RES_STATUS                      @"status"
#define RES_INTEREST                    @"interest"
#define RES_MOVIE                       @"movie"
#define RES_ABOUTME                     @"about_me"
#define RES_PHOTOURL                    @"photo_url"
#define RES_FILEURL                     @"file_url"
#define RES_FRIENDCOUNT                 @"friend_count"
#define RES_FRIENDLIST                  @"friend_list"
#define RES_CLASSNAME                   @"class_name"
#define RES_SEARCHRESULT                @"search_result"

#define RES_BLOCKLIST                   @"block_list"

#define PARAM_ID                        @"id"
#define PARAM_FILE                      @"file"

/**
 **     Response Code
 **/

#pragma mark -
#pragma mark - Response Code

#define CODE_SUCCESS                    0
#define CODE_EXISTEMAIL                 101
#define CODE_UNREGUSER                  102
#define CODE_WRONGPWD                   104
#define CODE_NOTEMAIL                   107
#define CODE_WRONGAUTH                  108


#endif /* ReqConst_h */




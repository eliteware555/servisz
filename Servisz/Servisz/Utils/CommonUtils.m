//
//  ImageUtil.m
//  DChat
//
//  Created by ChenJunLi on 12/16/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import "CommonUtils.h"
#import "UIImage+ResizeMagick.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation CommonUtils

-(id) init {
    
    self = [super init];
    if(self) {
        // custom init code
    }
    
    return self;
}

// resize image with specified size
+ (UIImage *) imageResize: (UIImage *) srcImage resizeTo:(CGSize) newSize {
    
    UIImage * resImage;
    
    resImage = [srcImage resizedImageByMagick:@"128x128#"];
    return resImage;
}

// save image to file (Documents/Campus/profile.png
+ (NSString *) saveToFile:(UIImage *) srcImage {
    
    NSString * savedPhotoURL = nil;
    
    NSString * outputFileName = @"";
    UIImage * outputImage;
    
    NSLog(@"scr width = %f, scr height = %f", srcImage.size.width, srcImage.size.height);
    
    // set output file name and resize source image with output image size
    outputFileName = @"profile.jpg";
    outputImage = [CommonUtils imageResize:srcImage resizeTo:CGSizeMake(PROFILE_IMG_SIZE, PROFILE_IMG_SIZE)];
    
    NSLog(@"out width = %f, out height = %f", outputImage.size.width, outputImage.size.height);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // current document directory
    [fileManager changeCurrentDirectoryPath:documentsDirectory];
    [fileManager createDirectoryAtPath:SAVE_ROOT_PATH withIntermediateDirectories:YES attributes:nil error:NULL];
    
    // Documents/Campus
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:SAVE_ROOT_PATH];
    NSString * filePath = [documentsDirectory stringByAppendingPathComponent:outputFileName];
    
    NSLog(@"save filePath = %@", filePath);
    
    // if the file exists already, delete and write, else if create filePath
    if([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:nil];
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    // Write a UIImage to PNG file
    //    [UIImagePNGRepresentation(outputImage) writeToFile:filePath atomically:YES];
    
    [UIImageJPEGRepresentation(outputImage, 1.0) writeToFile:filePath atomically:YES];
    
    NSError * error;
    [fileManager contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    return savedPhotoURL = filePath;
}

+ (void) setUserEmail: (NSString *) email {
    
    [UserDefault setStringValue:PREFKEY_USEREMAIL value:email];
}

+ (NSString *) getUserEmail {
    
    return [UserDefault getStringValue:PREFKEY_USEREMAIL];
}

+ (void) setUserPassword: (NSString *) password {

    [UserDefault setStringValue:PREFKEY_USERPWD value:password];
}

+ (NSString *) getUserPassword {
    
    return [UserDefault getStringValue:PREFKEY_USERPWD];
}

+ (void) setIsFromLogout:(BOOL) isFromLogout {
    
    [UserDefault setBoolValue:@"IS_FROM_LOGOUT" value:isFromLogout];
}

+ (BOOL) getIsFromLogout {
    return [UserDefault getBoolValue:@"IS_FROM_LOGOUT"];
}

+ (void) setXmppID:(int) _idx {
    
    [UserDefault setIntValue:PREFKEY_XMPPID value:_idx];
}

+ (int) getXmppID {
    
    return [UserDefault getIntValue:PREFKEY_XMPPID];
}

+ (void) setLastLoginEmail:(NSString *) _email {
    
    [UserDefault setStringValue:PREFKEY_LASTLOGINID value:_email];
}

+ (NSString *) getLastLoginEmail {
    
    return [UserDefault getStringValue:PREFKEY_LASTLOGINID];
}

+ (NSArray *) getMajors {
    
    return @[@"Business", @"Education", @"Engineering", @"Liberal Arts", @"Science", @"Architecture", @"Nursing", @"Social Work", @"Urban and Public", @"University College"];
}

+ (NSArray *) getYears {
    return @[@"Freshman", @"Sophomore", @"Junior", @"Senior", @"Graduate"];
}

+ (NSArray *) getSexs {
    return @[@"Male", @"Female", @"Trans"];
}

+ (NSArray *) getStatus {
    return @[@"Single", @"In a relationship", @"In an open relationship",
             @"Engaged", @"Married", @"It's complicated", @"Divorced", @"Widowed"];
}

+ (NSArray *) getInterest {
    
    return @[@"Men", @"Women", @"Both"];
}

+ (void) vibrate {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}


@end








//
//  ImageUtil.h
//  DChat
//
//  Created by ChenJunLi on 12/16/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "UserDefault.h"

@interface CommonUtils : NSObject

// save image to file with size specification
+(NSString *) saveToFile:(UIImage *) scrImage;

// save user email and password
+ (void) setUserEmail: (NSString *) email;
+ (NSString *) getUserEmail;

+ (void) setUserPassword: (NSString *) password;
+ (NSString *) getUserPassword;

// set logout
+ (void) setIsFromLogout:(BOOL) isFromLogout;
+ (BOOL) getIsFromLogout;

+ (void) setXmppID:(int) _idx;
+ (int) getXmppID;

+ (void) setLastLoginEmail:(NSString *) _email;
+ (NSString *) getLastLoginEmail;

+ (NSArray *) getMajors;
+ (NSArray *) getYears;
+ (NSArray *) getSexs;
+ (NSArray *) getStatus;
+ (NSArray *) getInterest;

+ (void) vibrate;

@end






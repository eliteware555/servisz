//
//  MyJobsCell.h
//  Servisz
//
//  Created by Victory on 10/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MaterialKit;

@interface ProjectCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *contentCardView;

@property (nonatomic, weak) IBOutlet MKButton *rippleButton;

@property (nonatomic, weak) IBOutlet UILabel *lblPostedTime;

@property (nonatomic, weak) IBOutlet UILabel *lblResult;

@end

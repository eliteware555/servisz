//
//  MyJobsCell.m
//  Servisz
//
//  Created by Victory on 10/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ProjectCell.h"

@implementation ProjectCell

- (void)awakeFromNib {
    // Initialization code
    
    self.contentCardView.layer.borderColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0].CGColor;
    
    self.rippleButton.rippleDuration = 0.2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

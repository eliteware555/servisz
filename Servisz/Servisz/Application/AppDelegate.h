//
//  AppDelegate.h
//  Servisz
//
//  Created by Victory on 02/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

